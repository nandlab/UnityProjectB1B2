using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonRPG
{
public class EnemyController : MonoBehaviour
{
    Animator animator;

    protected void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void GetHit()
    {
        animator.SetTrigger("Hit");
    }

    public void Attack()
    {
        animator.SetTrigger("Attack");
    }

    public void Special()
    {
        animator.SetTrigger("Special");
    }
}
}
