using UnityEngine;
using UnityEngine.InputSystem;

namespace DungeonRPG
{
public class TopDownPlayerController : MonoBehaviour
{ 
    [SerializeField] float speed = 5f;
    [SerializeField] float runMultiplier = 1.5f;
    Vector2 moveDirection = Vector2.zero;
    float modifier = 1.0f;
    Vector2 moveVector;

    static Vector2 lastPosition = Vector2.zero;

    Rigidbody2D rb;
    Animator anim;
    GameObject slime = null;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        UpdateMoveVector();
        rb.position = lastPosition;
    }

    private void FixedUpdate() => rb.velocity = moveVector * speed;

    private void Update()
    {
        // GetInput();
        SetAnimations();
    }

    /*
    // ========== !!! Change this method to either use the Input Manager or the Input System !!! ==========
    private void GetInput()
    {
        var moveX = 0;
        var moveY = 0;

        if (Input.GetKey(KeyCode.A)) moveX -= 1;
        if (Input.GetKey(KeyCode.D)) moveX += 1;
        if (Input.GetKey(KeyCode.S)) moveY -= 1;
        if (Input.GetKey(KeyCode.W)) moveY += 1;

        moveVector = new Vector2(moveX, moveY).normalized;
        if (Input.GetKey(KeyCode.LeftShift)) moveVector *= runMultiplier;
    }
    */

    private void UpdateMoveVector()
    {
        moveVector = moveDirection * modifier;
    }

    public void Move(InputAction.CallbackContext callbackContext)
    {
        moveDirection = callbackContext.ReadValue<Vector2>();
        UpdateMoveVector();
    }

    public void Run(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.started)
            modifier = runMultiplier;
        else if (callbackContext.canceled)
            modifier = 1.0f;
        UpdateMoveVector();
    }

    public void Interact(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.started && slime is not null)
        {
            lastPosition = rb.position;
            slime.GetComponent<Slime>().StartBattle();
        }
    }

    private void SetAnimations()
    { 
        // If the player is moving
        if (moveVector != Vector2.zero)
        {
            // Trigger transition to moving state
            anim.SetBool("IsMoving", true);

            // Set X and Y values for Blend Tree
            anim.SetFloat("MoveX", moveVector.x);
            anim.SetFloat("MoveY", moveVector.y);
        }
        else
            anim.SetBool("IsMoving", false);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.name == "Door1")
        {
            rb.position = new Vector2(0, 12);
        }
        else if (col.name == "Door2")
        {
            rb.position = new Vector2(0, 2);
        }
        else if (col.tag == "Slime")
        {
            slime = col.gameObject;
            Debug.Log("Slime entered");
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Slime")
        {
            slime = null;
            Debug.Log("Slime exited");
        }
    }
}
}
