using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonRPG
{
public class Slime : MonoBehaviour
{
    [SerializeField] GameObject enemyPrefab;

    public void StartBattle()
    {
        Battle.Start(enemyPrefab);
    }
}
}
