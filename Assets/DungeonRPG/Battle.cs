using UnityEngine;
using UnityEngine.SceneManagement;

namespace DungeonRPG
{
public class Battle : MonoBehaviour
{
    static GameObject enemyPrefab;

    EnemyController enemyController;

    public static void Start(GameObject enemyPrefab)
    {
        Battle.enemyPrefab = enemyPrefab;
        SceneManager.LoadScene("DungeonRPGBattle");
    }

    void Awake()
    {
        Debug.Assert(enemyPrefab is not null);
        enemyController = Instantiate(enemyPrefab).GetComponent<EnemyController>();
    }

    public void HitEnemy()
    {
        enemyController.GetHit();
    }

    public void EnemyAttack()
    {
        enemyController.Attack();
    }

    public void EnemySpecial()
    {
        enemyController.Special();
    }

    public void Return()
    {
        SceneManager.LoadScene("DungeonRPG");
    }
}
}
