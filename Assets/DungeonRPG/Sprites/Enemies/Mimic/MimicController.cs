using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonRPG
{
public class MimicController : EnemyController
{
    new ParticleSystem particleSystem;

    new void Awake()
    {
        base.Awake();
        particleSystem = GetComponent<ParticleSystem>();
    }

    void PlayParticleEffect()
    {
        particleSystem.Play();
    }
}
}
