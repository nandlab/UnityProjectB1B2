using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonRPG
{
public class TreeController : EnemyController
{
    new ParticleSystem particleSystem;

    new void Awake()
    {
        base.Awake();
        particleSystem = GetComponent<ParticleSystem>();
    }

    public void LaunchBirds()
    {
        particleSystem.Play();
    }
}
}
