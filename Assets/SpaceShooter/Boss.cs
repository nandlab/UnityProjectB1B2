using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
public class Boss : Enemy
{
    new Rigidbody2D rigidbody2D;

    [SerializeField] Vector3 bulletOffsetLeft;
    [SerializeField] Vector3 bulletOffsetRight;
    [SerializeField] GameObject bullet;
    [SerializeField] float speed;
    [SerializeField] float margin;
    [SerializeField] float healthPoints;
    [SerializeField] float shootInterval;
    float nextShootTime;
    AudioSource audioSourceLaser;
    AudioSource audioSourceDamaged;
    AudioSource audioSourceDestroyed;
    bool _destroyed = false;
    ParticleSystem[] particleSystems;

    bool shootSide = false;

    override public bool isBoss
    {
        get => true;
    }

    override public bool isDestroyed
    {
        get => _destroyed;
        protected set => _destroyed = value;
    }

    void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        AudioSource[] audioSources = GetComponents<AudioSource>();
        audioSourceLaser = audioSources[0];
        audioSourceDamaged = audioSources[1];
        audioSourceDestroyed = audioSources[2];
        particleSystems = GetComponents<ParticleSystem>();
        GameManager.instance.RegisterActiveEnemy(this);
    }

    void Explode()
    {
        foreach (ParticleSystem ps in particleSystems)
        {
            ps.Play();
        }
    }

    void Start()
    {
        Camera cam = Camera.main;
        float height = cam.orthographicSize;
        float width = height * cam.aspect;
        float xMin = margin - width;
        float xMax = width - margin;
        float yMin = margin;
        float yMax = height - margin;

        Vector2 pos = new Vector2();
        pos.x = xMin + Random.value * (xMax - xMin);
        pos.y = yMin + Random.value * (yMax - yMin);
        transform.localPosition = pos;

        int dir = Random.value > 0.5f ? -1 : 1;
        rigidbody2D.velocity = new Vector2(dir * speed, 0);
        nextShootTime = Time.time + shootInterval;
    }

    void FixedUpdate()
    {
        if (!isDestroyed)
        {
            Camera cam = Camera.main;
            float height = cam.orthographicSize;
            float width = height * cam.aspect;
            float xMin = margin - width;
            float xMax = width - margin;
            float yMin = margin;
            float yMax = height - margin;
            Rigidbody2D rb = rigidbody2D;
            Vector2 pos = rb.position;
            if (pos.x >= xMax)
            {
                pos.x = xMax;
                rb.velocity = new Vector2(-speed, 0);
            }
            else if (pos.x <= xMin)
            {
                pos.x = xMin;
                rb.velocity = new Vector2(speed, 0);
            }
            if (pos.y >= yMax)
            {
                pos.y = yMax;
            }
            else if (pos.y <= yMin)
            {
                pos.y = yMin;
            }
            rb.position = pos;
        }
    }

    void Update()
    {
        if (isDestroyed)
        {
            if ( !(
                audioSourceLaser.isPlaying
                || audioSourceDamaged.isPlaying
                || audioSourceDestroyed.isPlaying
            ))
            {
                Destroy(gameObject);
            }
        }
        else
        {
            float time = Time.time;
            if (time >= nextShootTime)
            {
                Fire();
                nextShootTime = time + shootInterval;
            }
        }
    }

    void Fire()
    {
        audioSourceLaser.Play();
        Instantiate(bullet, transform.position + (shootSide ? bulletOffsetLeft : bulletOffsetRight), Quaternion.identity);
        shootSide = !shootSide;
    }

    void Destroy()
    {
        isDestroyed = true;
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
        rigidbody2D.velocity = Vector2.zero;
        audioSourceDestroyed.Play();
        Explode();
        GameManager.instance.NotifyEnemyDestroyed(this);
        Debug.Log("Enemy destroyed");
    }

    override public void Damage(float healthPointsLost)
    {
        healthPoints -= healthPointsLost;
        if (healthPoints <= 0)
        {
            Destroy();
        }
        else if (healthPointsLost >= 1)
        {
            audioSourceDamaged.Play();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!isDestroyed)
        {
            if (other.gameObject.tag == "PlayerBullet")
            {
                Damage(1);
                Destroy(other.gameObject);
            }
        }
    }
}
}
