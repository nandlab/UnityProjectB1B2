using UnityEngine;
using TMPro;

namespace SpaceShooter
{
public class ScoreUI : MonoBehaviour
{
    private TextMeshProUGUI scoreText;

    void Awake()
    {
        scoreText = GetComponent<TextMeshProUGUI>();
    }

    public void showScore(int score)
    {
        scoreText.text = $"Score: {score}";
    }
}
}
