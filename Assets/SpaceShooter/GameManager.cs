using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SpaceShooter
{
public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject[] enemyVariants;
    [SerializeField] GameObject boss;
    float nextSpawnTime;
    int enemyCount = 0;
    int bossCount = 0;
    int regularEnemiesDestroyed = 0;
    int bossesDestroyed = 0;
    List<Enemy> _activeEnemies = new List<Enemy>();
    [SerializeField] UnityEvent<int> scoreUpdateEvent;
    [SerializeField] UnityEvent<int> highScoreUpdateEvent;
    int highScore = 0;

    public List<Enemy> activeEnemies {
        get => _activeEnemies;
        private set => _activeEnemies = value;
    }

    static public GameManager instance {
        get;
        private set;
    }

    GameObject RandomEnemy()
    {
        Debug.Assert(enemyVariants.Length > 0);
        int choice = Random.Range(0, enemyVariants.Length);
        return enemyVariants[choice];
    }

    void Awake()
    {
        Debug.Log("GameManager awaking");
        instance = this;
        highScore = PlayerPrefs.GetInt("highscore", 0);
        highScoreUpdateEvent.Invoke(highScore);
    }

    void Start()
    {
        Debug.Log("GameManager started");
        nextSpawnTime = Time.time + Random.Range(4.0f, 6.0f);
    }

    void SpawnEnemy()
    {
        Instantiate(RandomEnemy()).name = $"Enemy{enemyCount:00}";
        enemyCount++;
    }

    void SpawnBoss()
    {
        Instantiate(boss).name = $"Boss{bossCount:00}";
        bossCount++;
    }

    // Update is called once per frame
    void Update()
    {
        float time = Time.time;
        if (time >= nextSpawnTime)
        {
            SpawnEnemy();
            nextSpawnTime = time + Random.Range(4.0f, 6.0f);
        }
    }

    public void RegisterActiveEnemy(Enemy enemy)
    {
        activeEnemies.Add(enemy);
    }

    public void NotifyEnemyDestroyed(Enemy enemy)
    {
        activeEnemies.Remove(enemy);
        if (enemy.isBoss) {
            bossesDestroyed++;
        }
        else
        {
            regularEnemiesDestroyed++;
            if (regularEnemiesDestroyed % 10 == 0)
                SpawnBoss();
        }
        int score = regularEnemiesDestroyed + bossesDestroyed;
        if (score > highScore)
        {
            highScore = score;
            PlayerPrefs.SetInt("highscore", highScore);
            highScoreUpdateEvent.Invoke(highScore);
        }
        scoreUpdateEvent.Invoke(score);
    }
}
}
