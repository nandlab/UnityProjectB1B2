using UnityEngine;
using TMPro;

namespace SpaceShooter
{
public class HighScoreUI : MonoBehaviour
{
    private TextMeshProUGUI highScoreText;

    void Awake()
    {
        highScoreText = GetComponent<TextMeshProUGUI>();
    }

    public void showHighScore(int score)
    {
        highScoreText.text = $"High Score: {score}";
    }
}
}
