using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
public abstract class Enemy : MonoBehaviour
{
    abstract public bool isBoss
    {
        get;
    }

    abstract public bool isDestroyed
    {
        get;
        protected set;
    }

    abstract public void Damage(float healthPointsLost);
}
}
