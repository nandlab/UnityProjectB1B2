using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
public class Bullet : MonoBehaviour
{
    new Renderer renderer;
    
    [SerializeField] Vector2 moveVector2D;
    // [SerializeField] string targetTag;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = moveVector2D;
        renderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GeometryUtility.TestPlanesAABB(GeometryUtility.CalculateFrustumPlanes(Camera.main), renderer.bounds))
        {
            Destroy(gameObject);
        }
    }
}
}
