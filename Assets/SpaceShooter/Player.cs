using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace SpaceShooter
{
public class Player : MonoBehaviour
{
    new Rigidbody2D rigidbody2D;
    Vector2 moveVector2D = new Vector2();
    [SerializeField] GameObject bullet;
    [SerializeField] Vector3 bulletOffset;
    [SerializeField] float speed;
    [SerializeField] float margin;
    AudioSource audioSourceMusic;
    AudioSource audioSourceLaser;
    AudioSource audioSourceDestroyed;
    bool destroyed = false;
    SpriteRenderer spriteRenderer;
    LineRenderer laserBeam;
    bool trackingBeamAttack = false;
    ParticleSystem explosion;

    void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        AudioSource[] audioSources = GetComponents<AudioSource>();
        audioSourceMusic = audioSources[0];
        audioSourceLaser = audioSources[1];
        audioSourceDestroyed = audioSources[2];
        spriteRenderer = GetComponent<SpriteRenderer>();
        laserBeam = GetComponent<LineRenderer>();
        explosion = GetComponent<ParticleSystem>();
    }

    void FixedUpdate()
    {
        if (!destroyed)
        {
            Camera cam = Camera.main;
            float height = cam.orthographicSize;
            float width = height * cam.aspect;
            float xMin = margin - width;
            float xMax = width - margin;
            float yMin = margin - height;
            float yMax = - margin;
            Rigidbody2D rb = rigidbody2D;
            Vector2 actualMove = moveVector2D * speed;
            Vector2 pos = rb.position;
            if (pos.x >= xMax)
            {
                pos.x = xMax;
                actualMove.x = Mathf.Min(actualMove.x, 0.0f);
            }
            else if (pos.x <= xMin)
            {
                pos.x = xMin;
                actualMove.x = Mathf.Max(actualMove.x, 0.0f);
            }
            if (pos.y >= yMax)
            {
                pos.y = yMax;
                actualMove.y = Mathf.Min(actualMove.y, 0.0f);
            }
            else if (pos.y <= yMin)
            {
                pos.y = yMin;
                actualMove.y = Mathf.Max(actualMove.y, 0.0f);
            }
            rb.position = pos;
            rigidbody2D.velocity = actualMove;
        }
    }

    // Returns the nearest enemy or null if there are none.
    Enemy GetNearestEnemy()
    {
        float nearestDistance = float.PositiveInfinity;
        Enemy nearestEnemy = null;
        foreach (Enemy enemy in GameManager.instance.activeEnemies)
        {
            float distance = Vector3.Distance(enemy.transform.position, transform.position);
            if (distance < nearestDistance)
            {
                nearestDistance = distance;
                nearestEnemy = enemy;
            }
        }
        return nearestEnemy;
    }

    void Update()
    {
        if (destroyed)
        {
            if (!(audioSourceLaser.isPlaying || audioSourceDestroyed.isPlaying || explosion.isPlaying))
            {
                // Actually destroy the game object when all sounds finished playing.
                Destroy(gameObject);
                // Reload the scene.
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
        else
        {
            bool disableBeam = true;
            if (trackingBeamAttack)
            {
                Enemy nearestEnemy = GetNearestEnemy();
                if (nearestEnemy != null)
                {
                    disableBeam = false;
                    laserBeam.enabled = true;
                    laserBeam.SetPositions(new Vector3[] {nearestEnemy.transform.position, transform.position});
                    nearestEnemy.Damage(2 * Time.deltaTime);
                }
            }
            if (disableBeam)
            {
                laserBeam.enabled = false;
            }
        }
    }

    public void Move(InputAction.CallbackContext callbackContext)
    {
        if (!destroyed)
            moveVector2D = callbackContext.ReadValue<Vector2>();
    }

    public void Fire(InputAction.CallbackContext callbackContext)
    {
        if (!destroyed && callbackContext.started && callbackContext.ReadValue<float>() > 0)
        {
            Instantiate(bullet, transform.position + bulletOffset, Quaternion.identity);
            audioSourceLaser.Play();
        }
    }

    public void AltFire(InputAction.CallbackContext callbackContext)
    {
        if (!destroyed)
        {
            if (callbackContext.started)
            {
                trackingBeamAttack = true;
            }
            else if (callbackContext.canceled)
            {
                trackingBeamAttack = false;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!destroyed && other.gameObject.tag == "EnemyBullet")
        {
            trackingBeamAttack = false;
            laserBeam.enabled = false;
            spriteRenderer.enabled = false;
            audioSourceDestroyed.Play();
            explosion.Play();
            rigidbody2D.velocity = Vector2.zero;
            destroyed = true;
            Debug.Log("Player destroyed");
            audioSourceMusic.Stop();
            Destroy(other.gameObject);
        }
    }
}
}
